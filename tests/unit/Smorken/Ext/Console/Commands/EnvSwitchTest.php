<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/16/16
 * Time: 7:18 AM
 */
namespace Smorken\Ext\Console\Commands {

    function base_path($path = '')
    {
        return __DIR__ . ($path ? DIRECTORY_SEPARATOR . $path : $path);
    }

    use Illuminate\Console\Command;
    use Mockery as m;
    use Symfony\Component\Console\Helper\HelperSet;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Output\OutputInterface;
    use Symfony\Component\Console\Question\Question;

    class EnvSwitchTest extends \PHPUnit\Framework\TestCase
    {

        /**
         * @var \Illuminate\Contracts\Filesystem\Filesystem
         */
        protected $fs;

        public function setUp()
        {
            parent::setUp();
        }

        public function tearDown()
        {
            m::close();
        }

        public function testWithValidEnvironmentEnvExistsAndProdExists()
        {
            list($cmd, $fs, $laravel) = $this->getCommand();
            $fs->shouldReceive('exists')->once()->with(base_path('.env.testing'))->andReturn(true);
            $fs->shouldReceive('exists')->once()->with(base_path('.env'))->andReturn(true);
            $fs->shouldReceive('get')->once()->with(base_path('.env'))->andReturn("APP_ENV=prod\nFOO=bar");
            $fs->shouldReceive('copy')->once()->with(base_path('.env.testing'), base_path('.env'))->andReturn(true);
            $fs->shouldReceive('put')->once()->with(base_path('.env.backup'), "APP_ENV=prod\nFOO=bar")->andReturn(true);
            $o = $cmd->run(
                $input = new \Symfony\Component\Console\Input\ArrayInput(
                    [
                        'env' => 'testing',
                    ]
                ),
                $output = new \Symfony\Component\Console\Output\BufferedOutput()
            );
            $expected = "Copy successful.\nWriting backup data to .env.backup";
            $this->assertEquals(0, $o);
            $this->assertContains($expected, $output->fetch());
        }

        public function testWithValidEnvironmentEnvNotExistsAndExampleExists()
        {
            list($cmd, $fs, $laravel) = $this->getCommand();
            $fs->shouldReceive('exists')->once()->with(base_path('.env.testing'))->andReturn(false);
            $fs->shouldReceive('exists')->once()->with(base_path('.env.example'))->andReturn(true);
            $fs->shouldReceive('copy')->once()->with(base_path('.env.example'), base_path('.env.testing'))->andReturn(
                true
            );
            $o = $cmd->run(
                $input = new \Symfony\Component\Console\Input\ArrayInput(
                    [
                        'env' => 'testing',
                    ]
                ),
                $output = new \Symfony\Component\Console\Output\BufferedOutput()
            );
            $expected = '.env.testing and rerun this command.';
            $this->assertEquals(0, $o);
            $this->assertContains($expected, $output->fetch());
        }

        public function testWithValidEnvironmentEnvNotExistsAndExampleNotExists()
        {
            list($cmd, $fs, $laravel) = $this->getCommand();
            $fs->shouldReceive('exists')->once()->with(base_path('.env.testing'))->andReturn(false);
            $fs->shouldReceive('exists')->once()->with(base_path('.env.example'))->andReturn(false);
            $fs->shouldReceive('put')->once()->with(base_path('.env.testing'), 'APP_ENV=testing')->andReturn(true);
            $o = $cmd->run(
                $input = new \Symfony\Component\Console\Input\ArrayInput(
                    [
                        'env' => 'testing',
                    ]
                ),
                $output = new \Symfony\Component\Console\Output\BufferedOutput()
            );
            $expected = '.env.testing and rerun this command.';
            $this->assertEquals(0, $o);
            $this->assertContains($expected, $output->fetch());
        }

        public function testWithValidEnvironmentEnvExistsAndProdNotExists()
        {
            list($cmd, $fs, $laravel) = $this->getCommand();
            $fs->shouldReceive('exists')->once()->with(base_path('.env.testing'))->andReturn(true);
            $fs->shouldReceive('exists')->once()->with(base_path('.env'))->andReturn(false);
            $fs->shouldReceive('copy')->once()->with(base_path('.env.testing'), base_path('.env'))->andReturn(true);
            $o = $cmd->run(
                $input = new \Symfony\Component\Console\Input\ArrayInput(
                    [
                        'env' => 'testing',
                    ]
                ),
                $output = new \Symfony\Component\Console\Output\BufferedOutput()
            );
            $expected = 'Copy successful.';
            $this->assertEquals(0, $o);
            $this->assertContains($expected, $output->fetch());
        }

//        public function testWithQuestion()
//        {
//            // Not able to test interactive questions?
//            list($cmd, $fs, $laravel) = $this->getCommand();
//            $this->mockQuestions($cmd, function($text, $order, Question $question) {
//                // you can check against $text to see if this is the question you want to handle
//                // and you can check against $order (starts at 0) for the order the questions come in
//
//                // handle a question
//                if (strpos($text, 'overwrite') !== false) {
//                    return true;
//
//                    // handle another question
//                } elseif (strpos($text, 'api_key') !== false) {
//                    return 'bnet-api-test-key';
//                }
//
//                throw new \RuntimeException('Was asked for input on an unhandled question: '.$text);
//            });
//            $o = $cmd->run(
//                $input = new \Symfony\Component\Console\Input\ArrayInput([]),
//                $output = new \Symfony\Component\Console\Output\BufferedOutput()
//            );
//            echo $output->fetch();
//        }

        protected function mockQuestions(Command $cmd, callable $questions)
        {
            $ask = function (InputInterface $input, OutputInterface $output, Question $question) use ($questions) {
                static $order = -1;

                $order = $order + 1;
                $text = $question->getQuestion();

                $output->write($text." => ");
                $response = call_user_func($questions, $text, $order, $question);

                $output->writeln(print_r($response, true));
                return $response;
            };

            $helper = $this->getMock('\Symfony\Component\Console\Helper\QuestionHelper', ['ask']);
            $helper->expects($this->any())
                   ->method('ask')
                   ->will($this->returnCallback($ask));
            $cmd->setHelperSet(new HelperSet());
            $cmd->getHelperSet()->set($helper, 'question');
        }

        /**
         * @return array
         */
        protected function getCommand()
        {
            $laravel = m::mock(\Illuminate\Contracts\Foundation\Application::class);
            $fs = m::mock(\Illuminate\Contracts\Filesystem\Filesystem::class);
            $command = new \Smorken\Ext\Console\Commands\EnvSwitch($fs);
            $command->setLaravel($laravel);
            $laravel->shouldReceive('call')->with([$command, 'fire'])->andReturnUsing(
                function ($call) {
                    return call_user_func($call);
                }
            );
            $fs->shouldReceive('glob')->with(base_path('.env.*'))->andReturn(
                [
                    base_path('.env.example'),
                    base_path('.env.testing'),
                ]
            );
            return [$command, $fs, $laravel];
        }
    }
}
