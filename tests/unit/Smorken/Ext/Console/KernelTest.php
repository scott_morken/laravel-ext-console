<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/16/16
 * Time: 9:35 AM
 */
use Illuminate\Console\Scheduling\Schedule;
use Mockery as m;

class KernelTest extends \PHPUnit\Framework\TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testConsoleScheduleInstanceOfExtSchedule()
    {
        $app = m::mock(Illuminate\Contracts\Foundation\Application::class);
        $events = m::mock(Illuminate\Contracts\Events\Dispatcher::class);
        $app->shouldReceive('booted')->once()->andReturnUsing(
            function ($closure) {
                $closure();
                return true;
            }
        );
        $cache = m::mock(\Illuminate\Contracts\Cache\Repository::class);
        $app->shouldReceive('make')->once()->with(\Illuminate\Contracts\Cache\Repository::class)->andReturn($cache);
        $app->shouldReceive('instance')->once()->with(
            'Illuminate\Console\Scheduling\Schedule',
            m::type(\Smorken\Ext\Console\Scheduling\Schedule::class)
        );
        $sut = new KernelStubK($app, $events);
        $this->assertInstanceOf(\Smorken\Ext\Console\Kernel::class, $sut);
    }
}

class KernelStubK extends \Smorken\Ext\Console\Kernel
{
    /**
     * Define the application's command schedule.
     *
     * @return void
     */
    protected function defineConsoleSchedule()
    {
        $this->app->instance(
            Schedule::class,
            $schedule = new ScheduleStubK($this->app->make(\Illuminate\Contracts\Cache\Repository::class))
        );

        $this->schedule($schedule);
    }

    protected function schedule(Schedule $schedule)
    {
        $schedule
            ->command('mycommand:dosomething')
            ->everyFiveMinutes()
            ->emailErrorOutputTo('email@example.org');
    }
}

class ScheduleStubK extends \Smorken\Ext\Console\Scheduling\Schedule
{
    public function __construct()
    {
        $this->mutex = m::mock(\Illuminate\Console\Scheduling\Mutex::class);
    }
}
