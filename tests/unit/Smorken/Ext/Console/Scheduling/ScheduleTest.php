<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/16/16
 * Time: 9:29 AM
 */

use Mockery as m;

class ScheduleTest extends \PHPUnit\Framework\TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testExecReturnsExtEvent()
    {
        $sut = new ScheduleStubS();
        $e = $sut->exec('foo', ['biz' => 'baz']);
        $this->assertInstanceOf(\Smorken\Ext\Console\Scheduling\Event::class, $e);
    }
}

class ScheduleStubS extends \Smorken\Ext\Console\Scheduling\Schedule
{

    public function __construct()
    {
        $this->mutex = m::mock(\Illuminate\Console\Scheduling\Mutex::class);
    }
}
