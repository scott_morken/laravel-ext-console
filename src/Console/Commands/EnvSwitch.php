<?php namespace Smorken\Ext\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputArgument;

class EnvSwitch extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'env:switch {env?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Switch your .env file to a new environment.';

    /**
     * @var Filesystem
     */
    protected $filesystem;

    public function __construct(Filesystem $filesystem)
    {
        parent::__construct();
        $this->filesystem = $filesystem;
    }

    /**
     * Execute the console command.
     * queue needs to match a queue handler defined in
     * app/config/queue.php in the 'queue_handlers' array key
     * @return mixed
     */
    public function fire()
    {
        $env = $this->argument('env');
        $possible = $this->getPossibleEnvFiles($this->filesystem);
        if (!$env) {
            $env = $this->ask(
                sprintf(
                    'Which environment would you like to use? (Enter to exit) [%s]',
                    implode(', ', array_keys($possible))
                )
            );
            $env = Str::slug(trim($env));
        }
        if (!$env) {
            $this->info('Exiting... no environment selected.');
            exit(0);
        }
        $this->comment('Attempting to switch to ' . $env);
        $envfullpath = $this->getEnvFullPath($env, $possible);
        if ($this->filesystem->exists($envfullpath)) {
            return $this->copyFileToProduction($this->filesystem, $envfullpath);
        } else {
            $exampleenv = base_path('.env.example');
            $this->comment(".env.$env does not exist...");
            if ($this->filesystem->exists($exampleenv)) {
                return $this->copyToNonProduction($this->filesystem, $exampleenv, $envfullpath);
            } else {
                $this->comment("Creating empty .env.$env");
                if ($this->filesystem->put($envfullpath, "APP_ENV=$env")) {
                    $this->info("Please edit $envfullpath and rerun this command.");
                }
            }
        }
        return null;
    }

    protected function copyFileToProduction($fs, $from)
    {
        $prodenv = base_path('.env');
        $proddata = $this->getOriginalProdEnv($fs, $prodenv);
        if ($this->copyTo($fs, $from, $prodenv)) {
            $this->backupOriginalProdEnv($fs, $proddata);
            return true;
        } else {
            return false;
        }
    }

    protected function copyToNonProduction($fs, $from, $to)
    {
        $results = $this->copyTo($fs, $from, $to);
        if ($results) {
            $this->info("Please edit $to and rerun this command.");
        }
        return $results;
    }

    protected function copyTo($fs, $from, $to)
    {
        $this->comment("Attempting to copy $from to $to...");
        if ($fs->copy($from, $to)) {
            $this->comment('Copy successful.');
            return true;
        } else {
            $this->error('Copy failed!');
            return false;
        }
    }

    protected function getOriginalProdEnv($fs, $prodenv)
    {
        if ($fs->exists($prodenv)) {
            $this->comment('Reading .env for backup...');
            return $fs->get($prodenv);
        }
        return null;
    }

    protected function backupOriginalProdEnv($fs, $data)
    {
        if ($data) {
            $this->comment('Writing backup data to .env.backup');
            $fs->put(base_path('.env.backup'), $data);
        }
    }

    protected function getEnvFullPath($env, $possible)
    {
        if (isset($possible[$env])) {
            return $possible[$env];
        }
        return base_path('.env.') . $env;
    }

    protected function getPossibleEnvFiles($fs)
    {
        $files = [];
        $skip = ['example'];
        $all = $fs->glob(base_path('.env.*'));
        if ($all === false) {
            return $files;
        }
        foreach ($all as $file) {
            $shortfile = basename($file);
            $parts = explode('.', $shortfile);
            if (count($parts) === 3 &&
                filetype($file) === 'file' &&
                !in_array($parts[2], $skip)
            ) {
                $files[$parts[2]] = $file;
            }
        }
        return $files;
    }
}
