<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/26/15
 * Time: 2:08 PM
 */

namespace Smorken\Ext\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Contracts\Cache\Repository as Cache;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Smorken\Ext\Console\Scheduling\Schedule as AppSchedule;

class Kernel extends ConsoleKernel
{

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //\Smorken\Ext\Console\Commands\EnvSwitch::class,
        //\Smorken\Rbac\Console\Commands\RbacSuperCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @return void
     */
    protected function defineConsoleSchedule()
    {
        $this->app->instance(
            Schedule::class,
            $schedule = new AppSchedule($this->app->make(Cache::class))
        );

        $this->schedule($schedule);
    }

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /*$schedule
            ->command('mycommand:dosomething')
            ->everyFiveMinutes()
            ->sendOutputTo(storage_path() . '/logs/cron.log')
            ->emailErrorOutputTo(explode('|', env('ERROR_EMAIL', 'email@myemail.edu')));*/
    }
}
