<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/11/15
 * Time: 9:23 AM
 */

namespace Smorken\Ext\Console\Scheduling;

use Illuminate\Contracts\Container\Container;
use Symfony\Component\Process\Process;

class Event extends \Illuminate\Console\Scheduling\Event
{

    protected $result = 0;

    /**
     * Run the command in the foreground.
     *
     * @param  \Illuminate\Contracts\Container\Container  $container
     * @return void
     */
    protected function runCommandInForeground(Container $container)
    {
        $this->callBeforeCallbacks($container);

        $this->result = (new Process(
            $this->buildCommand(), base_path(), null, null, null
        ))->run();

        $this->callAfterCallbacks($container);
    }

    /**
     * E-mail the results of the scheduled operation.
     *
     * @param  array $addresses
     * @return $this
     *
     * @throws \LogicException
     */
    public function emailErrorOutputTo($addresses)
    {
        $send = ($this->result == 0 ? false : true);
        if ($send) {
            return $this->emailOutputTo($addresses);
        }
        return $this;
    }
}
