<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/11/15
 * Time: 9:35 AM
 */

namespace Smorken\Ext\Console\Scheduling;

class Schedule extends \Illuminate\Console\Scheduling\Schedule
{

    /**
     * Add a new command event to the schedule.
     *
     * @param  string $command
     * @param array $parameters
     * @return \Illuminate\Console\Scheduling\Event
     */
    public function exec($command, array $parameters = [])
    {
        if (count($parameters)) {
            $command .= ' ' . $this->compileParameters($parameters);
        }

        $this->events[] = $event = new Event($this->mutex, $command);

        return $event;
    }
}
